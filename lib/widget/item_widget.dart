import 'package:apptest_2_migi/Models/food.dart';
import 'package:flutter/material.dart';
class ItemWidget extends StatefulWidget {
  const ItemWidget({Key? key, required this.item}) : super(key: key);
  final Food item;
  @override
  State<ItemWidget> createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {
  @override
  Widget build(BuildContext context) {
    // print(widget.item.toString());
    return Card(
        child: ListTile(
          title: Row(
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: SizedBox(height: 90,width: 63,
                child: Image.network(widget.item.image),),
              ),
              Column(
                children: [
                  Padding(
                    padding:const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Container(
                        alignment: Alignment.centerLeft,
                        margin: const EdgeInsets.fromLTRB(3, 0, 0, 7),
                        child: SizedBox(
                          width: 200,
                          child: Text(
                            widget.item.Name,
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        )),
                  ),
                  Padding(
                    padding:const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Container(
                        alignment: Alignment.topLeft,
                        margin: const EdgeInsets.fromLTRB(0, 0, 190, 0),
                        child: Text(widget.item.Price,style: const TextStyle(color: Colors.red),)),
                  ),
                ],
              ),
              const Icon(Icons.shopping_cart, color: Colors.blue,)
            ],
          ),
        ),
      );
  }
}
