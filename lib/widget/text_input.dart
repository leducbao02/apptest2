import 'package:flutter/material.dart';
class TextInput extends StatefulWidget {
  const TextInput({Key? key, required this.textName, required this.hintText, required this.textEditingController}) : super(key: key);
  final String textName;
  final String hintText;
  final TextEditingController textEditingController;

  @override
  State<TextInput> createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(30,5,30,5),
      child: Column(
        children: [
          Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
              alignment: Alignment.centerLeft,
              child: Text(widget.textName)),

          SizedBox(height:55,
          child: TextField(
            controller: widget.textEditingController,
            decoration: InputDecoration(
                hintText: widget.hintText,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(color:  Colors.white)
                ),
            ),
          )
          )

        ],
      ),
    );
  }
}
