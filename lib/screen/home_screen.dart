import 'package:apptest_2_migi/Models/food.dart';
import 'package:apptest_2_migi/screen/add_screen.dart';
import 'package:apptest_2_migi/widget/item_widget.dart';
import 'package:flutter/material.dart';
class HomeScreen extends StatefulWidget {
  // static const routeName = '/Home';
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Food> list = [
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
    Food("Salad", "3", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/600px-Cat03.jpg?20160327035619", "a salad is a dissh"),
  ];

 stateList(List<Food> _list){
   setState(() {
     list = _list;
   });
 }
  @override
  Widget build(BuildContext context) {
    // print(list.length);
    return Scaffold(
      body: Column(
        children: [
          Container(
            color: Colors.blueAccent,
            height: 100,
            child: const Center(child: Text('HOME',style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold,fontSize: 20),)),
          ),

         Expanded(
           child: ListView.builder(
                  itemCount: list.length,
                  // shrinkWrap: true,
                  itemBuilder: (context, index) => ItemWidget( item: list[index]) ),
         ),

        ],
      ),
      floatingActionButton: FloatingActionButton(
      onPressed:(){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (bulider) => AddScreen(list: list, state: stateList,),
          ),
        );
        // Navigator.of(context).pushNamed(AddScreen.routeName,);
      },
      tooltip: 'Increment',
      child: const Icon(Icons.add),
    ),
    );
  }
}
