import 'package:apptest_2_migi/screen/home_screen.dart';
import 'package:apptest_2_migi/screen/oder_screen.dart';
import 'package:flutter/material.dart';

class ScreenNav extends StatefulWidget {
  const ScreenNav({Key? key}) : super(key: key);

  @override
  State<ScreenNav> createState() => _ScreenNavState();
}

class _ScreenNavState extends State<ScreenNav> {
  int _currenIndex = 0;

  var tab = [
    const HomeScreen(),
    const OderScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: tab[_currenIndex],
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home),
                label: "Home"
            ),
            BottomNavigationBarItem(icon: Icon(Icons.border_all_rounded),
              label: "Order",

            ),
          ],
          currentIndex: _currenIndex,
          onTap: (value) {
            setState(() {
              _currenIndex = value;
            });
          },
        )
    );
  }
}



