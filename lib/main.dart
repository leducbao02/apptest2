import 'package:apptest_2_migi/screen/navigator.dart';
import 'package:flutter/material.dart';

void main() {
  runApp( const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ScreenNav(),
  ));
}

